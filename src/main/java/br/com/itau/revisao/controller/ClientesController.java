package br.com.itau.revisao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.revisao.models.Aplicacao;
import br.com.itau.revisao.models.Cliente;
import br.com.itau.revisao.service.AplicacaoService;
import br.com.itau.revisao.service.ClienteService;


@RestController
public class ClientesController {
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private AplicacaoService aplicacaoService; 
	
	@GetMapping("/cliente")
	public Iterable<Cliente> listarFilmes() {
		return clienteService.obterClientes();
	}
	
	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cliente criarFilme(@RequestBody Cliente cliente) {
		return clienteService.criarCliente(cliente);
	}
	
	@DeleteMapping("/cliente/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarFilme(@PathVariable Long id) {
		clienteService.apagarCliente(id);
	}
	
	@PutMapping("/cliente/{id}")
	public Cliente editarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
		return clienteService.editarCliente(id, cliente);
	}
	
	@PostMapping("/cliente/{id}/aplicacao")
	public Aplicacao IncluirAplicacao(@PathVariable Long id, @RequestBody Aplicacao aplicacao) {
		return aplicacaoService.criarAplicacao(id, aplicacao);
	}
	@GetMapping("/cliente/{id}/aplicacao")
	public Iterable<Aplicacao> ListarAplicacao(@PathVariable Long id) {
		return aplicacaoService.obterAplicacao(id);
	}
}
