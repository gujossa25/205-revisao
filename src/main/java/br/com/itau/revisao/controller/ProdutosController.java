package br.com.itau.revisao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.revisao.models.Produto;
import br.com.itau.revisao.service.ProdutoService;

@RestController
public class ProdutosController {
	@Autowired
	private ProdutoService produtoService;
	
	@GetMapping("/produto")
	public Iterable<Produto> listarProdutos() {
		return produtoService.obterProdutos();	  
	}
	
//	@GetMapping("/Produto/{id}/filmes")
//	public ProdutoDTO listarFilmes(@PathVariable Long id) {
//		return produtoService.obterProduto(id);
//	}
	
	@PostMapping("/produto")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Produto criarProduto(@RequestBody Produto produto) {
		return produtoService.criarProduto(produto);
	}
	
	@DeleteMapping("/produto/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void apagarProduto(@PathVariable Long id) {
		produtoService.apagarProduto(id);
	}
	@PutMapping("/produto/{id}")
	public Produto editarProduto(@PathVariable Long id, @RequestBody Produto produto) {
		return produtoService.editarProduto(id, produto);
	}

}
