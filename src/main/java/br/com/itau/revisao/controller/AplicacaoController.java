package br.com.itau.revisao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.revisao.models.Aplicacao;
import br.com.itau.revisao.service.AplicacaoService;

@RestController
public class AplicacaoController {
	@Autowired
	private AplicacaoService aplicacaoService; 
	
	@PostMapping("/simulacao")
	public double IncluirAplicacao(@RequestBody Aplicacao aplicacao) {
		return aplicacaoService.SimularAplicacao(aplicacao);
	}
}
