package br.com.itau.revisao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.revisao.models.Aplicacao;
import br.com.itau.revisao.models.Cliente;
import br.com.itau.revisao.models.Produto;
import br.com.itau.revisao.repository.AplicacaoRepository;

@Service
public class AplicacaoService {
	@Autowired
	private AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	
	private ProdutoService produtoService;
	
	public Iterable<Aplicacao> obterAplicacao(Long idCliente) {
		System.out.println("Chamaram o listar filmes");

		return aplicacaoRepository.findAllByCliente_Id(idCliente);
	}
	
	public Aplicacao criarAplicacao(long idCliente, Aplicacao aplicacao) {
		Cliente cliente = new Cliente();
		cliente.setId(idCliente);
		aplicacao.setCliente(cliente);
		return aplicacaoRepository.save(aplicacao);
		
		
	}
	public double SimularAplicacao(Aplicacao aplicacao) {
	
		Produto produto = produtoService.encontraOuDaErro(aplicacao.getProduto().getId());
		
		return (double)Math.pow((produto.getRendimento() + 1), aplicacao.getMeses()) * aplicacao.getValor();
	}
	


}
