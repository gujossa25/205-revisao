package br.com.itau.revisao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.revisao.models.Produto;
import br.com.itau.revisao.repository.ProdutoRepository;

@Service     
public class ProdutoService {
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Iterable<Produto> obterProdutos() {
		return produtoRepository.findAll();
	}
	
	public Produto criarProduto(Produto produto) {
		return produtoRepository.save(produto);
	}
	
//	public produtoDTO obterproduto(Long id) {
//		return produtoRepository.findSummary(id);
//	}

	public void apagarProduto(Long id) {
		Produto produto = encontraOuDaErro(id);
		produtoRepository.delete(produto);
	}
	
	public Produto editarProduto(Long id, Produto produtoAtualizado) {
		Produto produto = encontraOuDaErro(id);
		
		produto.setNome(produtoAtualizado.getNome());
		produto.setRendimento(produtoAtualizado.getRendimento());
		
		return produtoRepository.save(produto);
	}
	
	public Produto encontraOuDaErro(Long id) {
		Optional<Produto> optional = produtoRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}
		
		return optional.get();
	}
}
