package br.com.itau.revisao.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.revisao.models.Aplicacao;
import br.com.itau.revisao.models.Cliente;
import br.com.itau.revisao.repository.AplicacaoRepository;
import br.com.itau.revisao.repository.ClienteRepository;



@Service
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private AplicacaoService aplicacaoService;
	
	public Iterable<Cliente> obterClientes() {
		System.out.println("Chamaram o listar filmes");

		return clienteRepository.findAll();
	}
	
	public Cliente criarCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
		
		
	}
	
	public void apagarCliente(Long id) {
		Cliente cliente = encontraOuDaErro(id);
		clienteRepository.delete(cliente);
	}
	
	public Cliente editarCliente(Long id, Cliente ClienteAtualizado) {
		Cliente cliente = encontraOuDaErro(id);
		
		cliente.setNome(ClienteAtualizado.getNome());
		
				
		return clienteRepository.save(cliente);
	}
	
	public Cliente encontraOuDaErro(Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cliente não encontrado");
		}
		
		return optional.get();
	}
	public Iterable<Aplicacao> obterAplicacao(Long idCliente) {
		return aplicacaoService.obterAplicacao(idCliente);
	}
	


	
}
