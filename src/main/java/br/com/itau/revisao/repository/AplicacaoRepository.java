package br.com.itau.revisao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.revisao.models.Aplicacao;

@Repository
public interface AplicacaoRepository extends CrudRepository<Aplicacao, Long>{
	Iterable<Aplicacao> findAllByCliente_Id(Long id);
}
