package br.com.itau.revisao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.revisao.models.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>{

}
