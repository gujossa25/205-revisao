package br.com.itau.revisao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.revisao.models.Produto;

	@Repository
	public interface ProdutoRepository extends CrudRepository<Produto, Long>{
}
