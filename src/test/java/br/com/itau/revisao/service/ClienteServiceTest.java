package br.com.itau.revisao.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.revisao.models.Cliente;
import br.com.itau.revisao.repository.ClienteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClienteService.class)
public class ClienteServiceTest {
	@Autowired
	ClienteService clienteService;
	
	@MockBean
	ClienteRepository clienteRepository;
	
	@Test
	public void deveListarTodosClientes() {
		Cliente cliente = new Cliente();
		
		List<Cliente> clientes = new ArrayList<>();
		
		clientes.add(cliente);
		
		when(clienteRepository.findAll()).thenReturn(clientes);
		
		List<Cliente> resultado = org.assertj.core.util.Lists.newArrayList(clienteService.obterClientes());
		
		assertNotNull(resultado.get(0));
	}
	@Test
	public void deveCriarUmCliente() {
		Cliente cliente = new Cliente();
		
		clienteService.criarCliente(cliente);
		
		Mockito.verify(clienteRepository).save(cliente);
		
	}
	@Test
	public void deveEditarUmCliente() {
		long id = 1L;
		String novoNome = "Guilherme";
		String novoCpf = "111.111.111-11";
		Cliente clienteBanco = criarCliente();
		Cliente clienteFront = criarCliente();
		clienteFront.setNome("Guilherme");
		
		when(clienteRepository.findById(1L)).thenReturn(Optional.of(clienteBanco));
		when(clienteRepository.save(clienteBanco)).thenReturn(clienteBanco);
		
		Cliente resultado = clienteService.editarCliente(id, clienteFront);
		
		assertNotNull(resultado);
		assertEquals(novoNome, resultado.getNome());
		assertNotEquals(novoCpf, resultado.getCpf());
		
	
		
		
	}
	@Test(expected = ResponseStatusException.class)
	public void deveLancarEceptionAoBuscarUmFilmes() {
		long id = 1L;
		
		Cliente clienteFront = criarCliente();
		
		clienteFront.setNome("Guilherme");
		
		when(clienteRepository.findById(1L)).thenReturn(Optional.empty());
			
		clienteService.editarCliente(id, clienteFront);
		
	}
	private Cliente criarCliente() {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Maria");
		cliente.setCpf("222.22.222-22");
		return cliente;
	}
	@Test
	public void deletarUmFilmes() {
		long id = 1L;
		Cliente cliente = new Cliente();
		
		when(clienteRepository.findById(1L)).thenReturn(Optional.of(cliente));
		
		clienteService.apagarCliente(id);;
		
		Mockito.verify(clienteRepository).delete(cliente);
		
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarEceptionAoDeletarUmFilmes() {
		long id = 1L;
		
		when(clienteRepository.findById(id)).thenReturn(Optional.empty());
			
		clienteService.apagarCliente(id);
		
	}
}
