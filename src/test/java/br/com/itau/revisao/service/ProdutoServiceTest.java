package br.com.itau.revisao.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.revisao.models.Produto;
import br.com.itau.revisao.repository.ProdutoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ProdutoService.class})
public class ProdutoServiceTest {
	@Autowired
	ProdutoService produtoService;
	
	@MockBean
	ProdutoRepository produtoRepository;
	
	@Test
	public void deveListarTodosOsProdutos() {
		//setup
		Produto produto = new Produto();
		
		List<Produto> produtos = new ArrayList<>();
		produtos.add(produto);
		
		when(produtoRepository.findAll()).thenReturn(produtos);
		
		//action
		List<Produto> resultado = Lists.newArrayList(produtoService.obterProdutos());
		
		//check
//		Iterator<Produto> iterador = r;esultado.iterator();		
//		Produto produtoResultado = iterador.next();
		
		assertNotNull(resultado.get(0));
		assertEquals(produto, resultado.get(0));
	}
	
//	@Test
//	public void deveListarTodosOsProdutosDeUmDiretor() {
//		//setup
//		long id = 1;
//		
//		ProdutoDTO produtoDTO = mock(ProdutoDTO.class);
//		List<ProdutoDTO> produtos = new ArrayList<>();
//		produtos.add(produtoDTO);
//		
//		when(produtoRepository.findAllByDiretor_Id(id)).thenReturn(produtos);
//		
//		//action
//		List<ProdutoDTO> resultado = Lists.newArrayList(produtoService.obterProdutos(id));
//		
//		//check
//		assertEquals(produtoDTO, resultado.get(0));
//	}
	
	@Test
	public void deveCriarUmProduto() {
		//setup
		Produto produto = new Produto();
		
		//action
		produtoService.criarProduto(produto);
		
		//check
		verify(produtoRepository).save(produto);
	}
	
	@Test
	public void deveEditarUmProduto() {
		//setup
		Long id = 1L;
		Long novoId = 10L;
		String novoNome = "PIC";
		Double novoRendimento = 0.00001;
		Produto produtoDoBanco = criarProduto(id);
		
		Produto produtoDoFront = criarProduto(id);
		produtoDoFront.setId(novoId);
		produtoDoFront.setNome(novoNome);
		produtoDoFront.setRendimento(novoRendimento);
		
		when(produtoRepository.findById(id)).thenReturn(Optional.of(produtoDoBanco));
		when(produtoRepository.save(produtoDoBanco)).thenReturn(produtoDoBanco);
		
		//action
		Produto resultado = produtoService.editarProduto(id, produtoDoFront);
		
		//check
		assertNotNull(resultado);
		verify(produtoRepository).save(produtoDoBanco);
		assertEquals(novoNome, resultado.getNome());
		assertEquals(novoRendimento, resultado.getRendimento());
		assertNotEquals(novoId, resultado.getId());
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOProduto() {
		//setup
		Long id = 1L;
		Long novoId = 10L;
		Produto produtoDoFront = criarProduto(novoId);
		
		when(produtoRepository.findById(id)).thenReturn(Optional.empty());
		
		//action
		produtoService.editarProduto(id, produtoDoFront);
	}
	
	private Produto criarProduto(long id) {
		Produto produto = new Produto();
		produto.setId(id);
		produto.setNome("PIC");
		produto.setRendimento(0.00001);
		
		return produto;
	}
}
